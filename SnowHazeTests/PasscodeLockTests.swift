//
//  PasscodeLockTests.swift
//  SnowHazeTests
//
//  Created by Bram Tukker on 17/06/2018.
//

import XCTest
@testable import SnowHaze

class PasscodeLockTests: XCTestCase {
    
    // MARK: - Set up properties
    static var databaseManager = XCTDatabaseManager()
    static var originalLockAfterTime: Double!
    static var didSetPasscode = false
    
    // MARK: - XCT functions
    override class func setUp() {
        super.setUp()
        
        if databaseManager.hasAccess {
            // Temporary stores current Lock After Time value.
            originalLockAfterTime = PasscodeLockTests.databaseManager.settings.value(for: passcodeLockAfterDurationKey).float!
        }
    }
    
    override class func tearDown() {
        if databaseManager.hasAccess {
            // Resets the database setting with the data before the tests started.
            databaseManager.settings.set(.float(originalLockAfterTime), for: passcodeLockAfterDurationKey)
        }
        
        super.tearDown()
    }
    
    override func tearDown() {
        if PasscodeLockTests.databaseManager.hasAccess && PasscodeLockTests.didSetPasscode {
            // Removes the Passcode Lock.
            let dispatchGroup = DispatchGroup()
            dispatchGroup.enter()
            
            PasscodeManager.shared.asyncRawSetAndUnlock(key: "", type: .alphanumeric, mode: .off) { _ in
                dispatchGroup.leave()
            }
            
            _ = dispatchGroup.wait(timeout: .now() + 1)
            PasscodeLockTests.didSetPasscode = false
        }
        
        super.tearDown()
    }
    
}

// MARK: - Lock After Time
extension PasscodeLockTests {
    
    func testSetLockAfterTimeToInstantly() {
        guard SearchEngineTests.databaseManager.hasAccess else {
            XCTFail("Failed because the device has a Passcode Lock.")
            return
        }
        
        // Arrange
        let value: Double = 0
        
        let expected = value
        let actual: Double
        
        // Act
        PasscodeSettingsManager().setLockAfterTime(with: value)
        
        actual = PasscodeLockTests.databaseManager.settings.value(for: passcodeLockAfterDurationKey).float!
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testSetLockAfterTimeToLaunch() {
        guard SearchEngineTests.databaseManager.hasAccess else {
            XCTFail("Failed because the device has a Passcode Lock.")
            return
        }
        
        // Arrange
        let value: Double = .infinity
        
        let expected = value
        let actual: Double
        
        // Act
        PasscodeSettingsManager().setLockAfterTime(with: value)
        
        actual = PasscodeLockTests.databaseManager.settings.value(for: passcodeLockAfterDurationKey).float!
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testSetLockAfterTimeToNegative() {
        guard SearchEngineTests.databaseManager.hasAccess else {
            XCTFail("Failed because the device has a Passcode Lock.")
            return
        }
        
        // Arrange
        let value: Double = -1
        
        let expected: Double = 0
        let actual: Double
        
        // Act
        PasscodeSettingsManager().setLockAfterTime(with: value)
        
        actual = PasscodeLockTests.databaseManager.settings.value(for: passcodeLockAfterDurationKey).float!
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
}

// MARK: - Set Passcode Lock
extension PasscodeLockTests {
    
    // Note: the Passcode Type is not worth testing, since it only changes the virtual keyboard of the device.
    
    func setupPasscode(with key: String, and verifyKey: String, shouldResultIn: Bool) {
        guard SearchEngineTests.databaseManager.hasAccess else {
            XCTFail("Failed because the device has a Passcode Lock.")
            return
        }
        
        // Arrange
        let mode: PasscodeManager.LockingMode = .pinOnly
        let type: PasscodeController.PasscodeType = .alphanumeric
        
        let exp = expectation(description: "Store and verify key.")
        
        // Act
        // Stores the key.
        PasscodeManager.shared.asyncRawSetAndUnlock(key: key, type: type, mode: mode) { success in
            guard success else {
                XCTFail()
                return
            }
            
            PasscodeLockTests.didSetPasscode = true
            
            // Verifies the key.
            verifyDBKey(verifyKey) { success in
                guard success == shouldResultIn else {
                    XCTFail()
                    return
                }
                
                exp.fulfill()
            }
        }
        
        // Assert
        waitForExpectations(timeout: 5)
    }
    
    func testSetPasscodeWithKeyCorrect() {
        let key = "1"
        setupPasscode(with: key, and: key, shouldResultIn: true)
    }
    
    func testSetPasscodeWithKeyWrong() {
        let key = "1"
        let wrongKey = "2"
        setupPasscode(with: key, and: wrongKey, shouldResultIn: false)
    }
    
    func testSetPasscodeWithCapitalCorrect() {
        let key = "QWERTY"
        setupPasscode(with: key, and: key, shouldResultIn: true)
    }
    
    func testSetPasscodeWithCapitalWrong() {
        let key = "QWERTY"
        let wrongKey = "qwerty"
        setupPasscode(with: key, and: wrongKey, shouldResultIn: false)
    }
    
    func testSetPasscodeWithSymbolsCorrect() {
        let key = "!@#$"
        setupPasscode(with: key, and: key, shouldResultIn: true)
    }
    
    func testSetPasscodeWithSymbolsWrong() {
        let key = "!@#$"
        let wrongKey = "$%^&"
        setupPasscode(with: key, and: wrongKey, shouldResultIn: false)
    }
    
    func testSetPasscodeWithEmojiCorrect() {
        let key = "😀"
        setupPasscode(with: key, and: key, shouldResultIn: true)
    }
    
    func testSetPasscodeWithEmojiWrong() {
        let key = "😀"
        let wrongKey = "U+1F600"
        setupPasscode(with: key, and: wrongKey, shouldResultIn: false)
    }
    
}
