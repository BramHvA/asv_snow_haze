//
//  SearchEngineTests.swift
//  SnowHazeTests
//
//  Created by Bram Tukker on 17/06/2018.
//

import XCTest
@testable import SnowHaze

class SearchEngineTests: XCTestCase {
    
    // MARK: - Set up properties
    static var databaseManager = XCTDatabaseManager()
    static var originalSearchEnginge: SearchEngineType!
    static var originalSuggestionEnginges: [SearchEngineType]!
    
    // MARK: - XCT functions
    override class func setUp() {
        super.setUp()
        
        if databaseManager.hasAccess {
            // Temporary stores current Search Engines.
            originalSearchEnginge = SearchEngineType(rawValue: databaseManager.settings.value(for: searchEngineKey).integer!)!
            originalSuggestionEnginges = SearchEngine.decode(databaseManager.settings.value(for: searchSuggestionEnginesKey).text!)
        }
    }
    
    override class func tearDown() {
        if databaseManager.hasAccess {
            // Resets the database setting with the data before the tests started.
            let newSearchEngine = originalSearchEnginge!
            let oldSearchEngine = SearchEngineType(rawValue: databaseManager.settings.value(for: searchEngineKey).integer!)!
            
            let newSuggestionEngines = originalSuggestionEnginges!
            let oldSuggestionEngines = SearchEngine.decode(databaseManager.settings.value(for: searchSuggestionEnginesKey).text!)
            
            if newSearchEngine != oldSearchEngine {
                databaseManager.settings.set(.integer(newSearchEngine.rawValue), for: searchEngineKey)
            }
            
            if newSuggestionEngines != oldSuggestionEngines {
                databaseManager.settings.set(.text(SearchEngine.encode(newSuggestionEngines)), for: searchSuggestionEnginesKey)
            }
        }
        
        super.tearDown()
    }
    
}

// MARK: - Search Engine Type
extension SearchEngineTests {
    
    func testGetSearchEngineTypeFirst() {
        // Arrange
        let expected: SearchEngineType = .none
        let actual: SearchEngineType?
        
        // Act
        actual = SearchEngineType(rawValue: 0)
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testGetSearchEngineTypesLast() {
        // Arrange
        let expected: SearchEngineType = .snowhaze
        let actual: SearchEngineType?
        
        // Act
        actual = SearchEngineType(rawValue: 10)
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testGetSearchEngineTypeStartBounds() {
        // Arrange
        let actual: SearchEngineType?
        
        // Act
        actual = SearchEngineType(rawValue: -1)
        
        // Assert
        XCTAssertNil(actual)
    }
    
    func testGetSearchEngineTypeEndBounds() {
        // Arrange
        let actual: SearchEngineType?
        
        // Act
        actual = SearchEngineType(rawValue: 11)
        
        // Assert
        XCTAssertNil(actual)
    }
    
    func testGetSearchEngineTypeFirstInverse() {
        // Arrange
        let value: Int64 = 0
        
        let expected: Int64 = value
        let actual: Int64?
        
        // Act
        let type = SearchEngineType(rawValue: value)
        actual = type?.rawValue
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testGetSearchEngineTypesLastInverse() {
        // Arrange
        let value: Int64 = 10
        
        let expected: Int64 = value
        let actual: Int64?
        
        // Act
        let type = SearchEngineType(rawValue: value)
        actual = type?.rawValue
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testGetSearchEngineTypeStartBoundsInverse() {
        // Arrange
        let value: Int64 = -1
        
        let actual: Int64?
        
        // Act
        let type = SearchEngineType(rawValue: value)
        actual = type?.rawValue
        
        // Assert
        XCTAssertNil(actual)
    }
    
    func testGetSearchEngineTypeEndBoundsInverse() {
        // Arrange
        let value: Int64 = 11
        
        let actual: Int64?
        
        // Act
        let type = SearchEngineType(rawValue: value)
        actual = type?.rawValue
        
        // Assert
        XCTAssertNil(actual)
    }
    
}

// MARK: - Update Suggestion Engine
extension SearchEngineTests {
    
    func testUpdateSuggestionEngineNone() {
        // Arrange
        let new: SearchEngineType = .bing
        let old: SearchEngineType = .google
        let inList: [SearchEngineType] = []
        
        let actual: [SearchEngineType]
        
        // Act
        actual = SearchEngine.updateSuggestionEngine(new: new, old: old, inList: inList)
        
        // Assert
        XCTAssertTrue(actual.isEmpty)
    }
    
    func testUpdateSuggestionEngineMain() {
        // Arrange
        let new: SearchEngineType = .bing
        let old: SearchEngineType = .google
        let inList = [old]
        
        let expected = [new]
        let actual: [SearchEngineType]
        
        // Act
        actual = SearchEngine.updateSuggestionEngine(new: new, old: old, inList: inList)
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testUpdateSuggestionEngineMultiple() {
        // Arrange
        let new: SearchEngineType = .bing
        let old: SearchEngineType = .google
        let current: SearchEngineType = .wikipedia
        let inList = [old, current]
        
        let expected = [new, current]
        let actual: [SearchEngineType]
        
        // Act
        actual = SearchEngine.updateSuggestionEngine(new: new, old: old, inList: inList)
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testUpdateSuggestionEngineSame() {
        // Arrange
        let new: SearchEngineType = .google
        let old: SearchEngineType = .google
        let inList = [old]
        
        let expected = [new]
        let actual: [SearchEngineType]
        
        // Act
        actual = SearchEngine.updateSuggestionEngine(new: new, old: old, inList: inList)
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
}

// MARK: - Update Database
extension SearchEngineTests {
    
    func testStoreSearchEngine() {
        guard SearchEngineTests.databaseManager.hasAccess else {
            XCTFail("Failed because the device has a Passcode Lock.")
            return
        }
        
        // Arrange
        let engine = SearchEngineType.google.rawValue
        
        let expected = engine
        let actual: Int64
        
        // Act
        SearchEngineTests.databaseManager.settings.set(.integer(engine), for: searchEngineKey)
        
        actual = SearchEngineTests.databaseManager.settings.value(for: searchEngineKey).integer!
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
    func testStoreSuggestionEngine() {
        guard SearchEngineTests.databaseManager.hasAccess else {
            XCTFail("Failed because the device has a Passcode Lock.")
            return
        }
        
        // Arrange
        let suggestionEngine: [SearchEngineType] = [.google]
        
        let expected = suggestionEngine
        let actual: [SearchEngineType]
        
        // Act
        SearchEngineTests.databaseManager.settings.set(.text(SearchEngine.encode(suggestionEngine)), for: searchSuggestionEnginesKey)
        
        actual = SearchEngine.decode(SearchEngineTests.databaseManager.settings.value(for: searchSuggestionEnginesKey).text!)
        
        // Assert
        XCTAssertEqual(expected, actual)
    }
    
}
