//
//  WebsiteDataTest.swift
//  SnowHazeTests
//
//  Created by Richard Essemiah on 21/06/2018.
//

import XCTest
@testable import SnowHaze

class WebsiteDataTest: XCTestCase {
	
    let store = WKWebsiteDataStore.default()
    
	func testClearAllData() {
		let types = WKWebsiteDataStore.allWebsiteDataTypes()
		var amountOfData = 0
		
		let exp = expectation(description: "Deleting website data")
		store.fetchDataRecords(ofTypes: types) { records in
			print("Amount of records are \(records.count)")
			print("Records are \(records.map { $0.displayName})")
			self.store.removeData(ofTypes: types, for: records) {}
			amountOfData = records.count
			exp.fulfill()
		}
		
		waitForExpectations(timeout: 10)
		XCTAssertEqual(amountOfData, 0)
	}
	
	func testClearCache() {
		let types = Set<String>(arrayLiteral: WKWebsiteDataTypeDiskCache, WKWebsiteDataTypeMemoryCache, WKWebsiteDataTypeOfflineWebApplicationCache)
		var amountOfData = 0
		
		let exp = expectation(description: "Deleting website data")
		store.fetchDataRecords(ofTypes: types) { records in
			print("Amount of records are \(records.count)")
			print("Records are \(records.map { $0.displayName})")
			self.store.removeData(ofTypes: types, for: records) {}
			amountOfData = records.count
			exp.fulfill()
			
		}
		
		waitForExpectations(timeout: 10)
		XCTAssertEqual(amountOfData, 0)
	}
	
	func testClearCookies() {
		let types = Set<String>(arrayLiteral: WKWebsiteDataTypeCookies)
		var amountOfData = 0
		
		let exp = expectation(description: "Deleting website data")
		store.fetchDataRecords(ofTypes: types) { records in
			print("Amount of records are \(records.count)")
			print("Records are \(records.map { $0.displayName})")
			self.store.removeData(ofTypes: types, for: records) {}
			amountOfData = records.count
			exp.fulfill()
			
		}
		
		waitForExpectations(timeout: 5)
		XCTAssertEqual(amountOfData, 0)
	}
	
}
