//
//  XCTDatabaseManager.swift
//  SnowHazeTests
//
//  Created by Bram Tukker on 19/06/2018.
//

@testable import SnowHaze

class XCTDatabaseManager {
    
    var settings: SettingsDefaultWrapper!
    var hasAccess: Bool
    
    init() {
        // Tries to gain access to database. Will fail if the device has a Passcode Lock.
        keyingData = try! pbkdf("")
        hasAccess = SQLCipher(path: dbPath, key: keyingData) != nil
        
        if hasAccess {
            // Retrieves settings.
            _ = PolicyManager.globalManager()
            settings = SettingsDefaultWrapper.wrapGlobalSettings()
        }
    }
    
}
